<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

	protected function template($view, $data = array())
	{
		$this->load->view('templates/header', $data);
		$this->load->view($view, $data);
		$this->load->view('templates/footer', $data);
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */