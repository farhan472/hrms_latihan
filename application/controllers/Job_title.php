<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job_title extends MY_Controller {
    public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('master_data_m/Job_title_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
		);
	}

	public function index(){
        
        $this->template('master_data/job_title/job_title_v');
    }
    
    public function dataJobTitle(){
		$get = $this->input->get();

		$limit = !empty($get['limit']) ? $get['limit'] : 0;
		$search = !empty($get['search']) ? $get['search'] : "";
		$offset = !empty($get['offset']) ? $get['offset'] : 0;
		$sort = !empty($get['sort']) ? $get['sort'] : "id";
		$order = !empty($get['order']) ? $get['order'] : "asc";

		$data['total'] = $this->Job_title_m->getJobTitle("", 0, 0, $search)->num_rows();

		$this->db->order_by($sort, $order);
				
		foreach ($data['rows'] as $key => $value) {

			$data['rows'][$key]['id'] = str_replace(['/', '=', '+', '@'], ['miringmiring', 'samasama', 'plusplus', 'et'],
			$this->encryption->encrypt($value['id']));

            // print_r($data['rows'][$key]['id']); die();
        }
		echo json_encode($data);
    }

    public function tambahJobTitle(){
        $this->template('master_data/job_title/tambah_job_title_v');
    }

    public function submitTambahJobTitle(){
		$this->form_validation->set_rules('nama_jobtitle', 'Nama Job Title', 'required|trim');
        $post_xss = $this->security->xss_clean($this->input->post());
       
        if ($this->form_validation->run() == FALSE)
        {
            $this->template('master_data/posisi/tambah_job_title_v');

        } else 
        {
			$data = ["nama_jobtitle" => $post_xss["nama_jobtitle"]
                    ];

			$insert = $this->Job_title_m->insertJobTitle($data);

        	if($insert) {

	            $this->session->set_flashdata('status', 'Berhasil menambah job title');
	            redirect(site_url('Job_title'));

            } else 
            {
        		$this->session->set_flashdata('status', 'Gagal menambah job title');
                $this->template('master_data/job_title/tambah_job_title_v');
                
        	}
	    }
	}

	public function editJobTitle($id){
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));

		$data['data'] = $this->Job_title_m->getJobTitle($decrypt_id)->row_array();
		//$data['data'] = $this->Posisi_m->getPosisi($decrypt_id)->row();
		
		// $decrypt_email = $data['data']['email'];
		// $decrypt_email = $data['data']->email;
		
		// $data['email_asli'] = $this->encryption->decrypt($decrypt_email);
		$this->template('master_data/job_title/edit_job_title_v', $data);

	}

	public function submitEditJobTitle(){
		$this->form_validation->set_rules('nama_jobtitle', 'Nama Job Title', 'required|trim');
		$id = $this->input->post('id');
        
        $post_xss = $this->security->xss_clean($this->input->post());
       
        if ($this->form_validation->run() == FALSE)
        {
            $this->template('master_data/job_title/edit_job_title_v');

        } else 
        {
			$data =	 ["nama_jobtitle" => $post_xss["nama_jobtitle"]
                    ];

			$update = $this->Job_title_m->updateJobTitle($id, $data);

        	if($update) {

	            $this->session->set_flashdata('status', 'Berhasil mengubah job title');
	            redirect(site_url('Job_title'));

            } else 
            {
        		$this->session->set_flashdata('status', 'Gagal mengubah job title');
                $this->template('master_data/job_title/edit_job_title_v');
                
        	}
	    }
	}
}
