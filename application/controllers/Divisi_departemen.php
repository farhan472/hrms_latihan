<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisi_departemen extends MY_Controller {
    public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('master_data_m/Divisi_departemen_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
		);
	}

	public function index(){
        
        $this->template('master_data/divisi_departemen/divisi_departemen_v');
    }
    
    public function dataDivisiDepartemen(){
		$get = $this->input->get();

		$limit = !empty($get['limit']) ? $get['limit'] : 0;
		$search = !empty($get['search']) ? $get['search'] : "";
		$offset = !empty($get['offset']) ? $get['offset'] : 0;
		$sort = !empty($get['sort']) ? $get['sort'] : "id";
		$order = !empty($get['order']) ? $get['order'] : "asc";

		$data['total'] = $this->Divisi_departemen_m->getDivisiDepartemen("", 0, 0, $search)->num_rows();

		$this->db->order_by($sort, $order);
				
		foreach ($data['rows'] as $key => $value) {

			$data['rows'][$key]['id'] = str_replace(['/', '=', '+', '@'], ['miringmiring', 'samasama', 'plusplus', 'et'],
			$this->encryption->encrypt($value['id']));

            // print_r($data['rows'][$key]['id']); die();
        }
		echo json_encode($data);
    }

    public function tambahDivisiDepartemen(){
        $this->template('master_data/divisi_departemen/tambah_divisi_departemen_v');
    }

    public function submitTambahDivisiDepartemen(){
      $this->form_validation->set_rules('nama_divisi', 'Nama Divisi', 'required|trim');
      $this->form_validation->set_rules('kode_divisi', 'Kode Divisi', 'required|trim');
        $post_xss = $this->security->xss_clean($this->input->post());
       
        if ($this->form_validation->run() == FALSE)
        {
            $this->template('master_data/divisi_departemen/tambah_divisi_departemen_v');

        } else 
        {
         $data = ["nama_divisi" => $post_xss["nama_jobtitle"],
                  "kode_divisi" => $post_xss["kode_divisi"]
                    ];

			$insert = $this->Divisi_departemen_m->insertDivisiDepartemen($data);

        	if($insert) {

	            $this->session->set_flashdata('status', 'Berhasil menambah divisi');
	            redirect(site_url('Divisi_departemen'));

            } else 
            {
        		$this->session->set_flashdata('status', 'Gagal menambah divisi');
                $this->template('master_data/divisi_departemen/tambah_divisi_departemen_v');
                
        	}
	    }
	}

	public function editDivisiDepartemen($id){
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));

		$data['data'] = $this->Divisi_departemen_m->getDivisiDepartemen($decrypt_id)->row_array();
		//$data['data'] = $this->Posisi_m->getPosisi($decrypt_id)->row();
		
		// $decrypt_email = $data['data']['email'];
		// $decrypt_email = $data['data']->email;
		
		// $data['email_asli'] = $this->encryption->decrypt($decrypt_email);
		$this->template('master_data/divisi_departemen/edit_divisi_departemen_v', $data);

	}

	public function submitEditDivisiDepartemen(){
      $this->form_validation->set_rules('nama_divisi', 'Nama Job Title', 'required|trim');
      $this->form_validation->set_rules('kode_divisi', 'Nama Job Title', 'required|trim');
		$id = $this->input->post('id');
        
        $post_xss = $this->security->xss_clean($this->input->post());
       
        if ($this->form_validation->run() == FALSE)
        {
            $this->template('master_data/divisi_departemen/edit_divisi_departemen_v');

        } else 
        {
         $data =	 ["nama_divisi" => $post_xss["nama_divisi"],
                     "kode_divisi" => $post_xss["kode_divisi"]
                    ];

			$update = $this->Divisi_departemen_m->updateDivisiDepartemen($id, $data);

        	if($update) {

	            $this->session->set_flashdata('status', 'Berhasil mengubah divisi');
	            redirect(site_url('Divisi_departemen'));

            } else 
            {
        		$this->session->set_flashdata('status', 'Gagal mengubah divisi');
                $this->template('master_data/divisi_departemen/edit_divisi_departemen_v');
                
        	}
	    }
	}
}
