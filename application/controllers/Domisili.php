<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Domisili extends MY_Controller
{

   public function index()
   {
      $data['judul'] = 'Domisili';
      $this->template('domisili_v', $data);
   }
}
