<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Posisi extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('master_data_m/Posisi_m');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-192',
				'mode' => 'cfb',
				'key' => '8XtTlwWOpEVjBepLqRJGKroKIvUBUyqI'
			)
		);
	}

	public function index()
	{

		$this->template('master_data/posisi/posisi_v');
	}

	public function dataPosisi()
	{
		$get = $this->input->get();

		$limit = !empty($get['limit']) ? $get['limit'] : 0;
		$search = !empty($get['search']) ? $get['search'] : "";
		$offset = !empty($get['offset']) ? $get['offset'] : 0;
		$sort = !empty($get['sort']) ? $get['sort'] : "ID";
		$order = !empty($get['order']) ? $get['order'] : "asc";

		$data['total'] = $this->Posisi_m->getPosisi("", 0, 0, $search)->num_rows();

		$this->db->order_by($sort, $order);

		$data['rows'] = $this->Posisi_m->getPosisi("", $limit, $offset, $search)->result_array();
		
		foreach ($data['rows'] as $key => $value) {

			$data['rows'][$key]['ID'] = str_replace(
				['/', '=', '+', '@'],
				['miringmiring', 'samasama', 'plusplus', 'et'],
				$this->encryption->encrypt($value['ID'])
			);

			// $data['rows'][$key]['email'] = $this->encryption->decrypt($value['email']);
			// print_r($data['rows'][$key]['id']); die();
		}
		echo json_encode($data);
	}

	public function tambahPosisi()
	{
		$data['dataDivisiDepartemen'] = $this->Posisi_m->getDivisiDepartemen()->result_array();
		$data['dataJobTitle'] = $this->Posisi_m->getJobTitle()->result_array();
		$this->template('master_data/posisi/tambah_posisi_v', $data);
	}

	public function submitTambahPosisi()
	{
		$this->form_validation->set_rules('job_title', 'Nama Job Title', 'required|trim');
		$this->form_validation->set_rules('divisi_departemen', 'Nama Divisi/Departemen', 'required|trim');
		$this->form_validation->set_rules('nama_posisi', 'Nama Posisi', 'required|trim');
		// $this->form_validation->set_rules('email', 'Email', 'valid_email|required|trim');
		
		$post_xss = $this->security->xss_clean($this->input->post());

		if ($this->form_validation->run() == FALSE) {
			$this->template('master_data/posisi/tambah_posisi_v');
		} else {
			//$encrypt_email = $this->encryption->encrypt($post_xss["email"]);
			$data = [
				"JOB_TITLE_ID" => $post_xss["job_title"],
				"DIVISI_ID" => $post_xss["divisi_departemen"],
				"NAMA_POSISI" => $post_xss["nama_posisi"],
				//  "created_datetime" => date('Y-m-d H:i:s')
				// "email" => $encrypt_email,
			];

			$insert = $this->Posisi_m->insertPosisi($data);

			if ($insert) {

				$this->session->set_flashdata('status', 'Berhasil menambah Posisi');
				redirect(site_url('Posisi'));
			} else {
				$this->session->set_flashdata('status', 'Gagal menambah Posisi');
				$this->template('master_data/posisi/tambah_posisi_v');
			}
		}
	}

	public function editPosisi($id)
	{
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));

		$data['data'] = $this->Posisi_m->editPosisi($decrypt_id)->row_array();
		$data['id'] = $id;
		$data['dataDivisiDepartemen'] = $this->Posisi_m->getDivisiDepartemen()->result_array();
		$data['dataJobTitle'] = $this->Posisi_m->getJobTitle()->result_array();
		
		//$data['data'] = $this->Posisi_m->getPosisi($decrypt_id)->row();

		// $decrypt_email = $data['data']['email'];
		// $decrypt_email = $data['data']->email;

		// $data['email_asli'] = $this->encryption->decrypt($decrypt_email);
		$this->template('master_data/posisi/edit_posisi_v', $data);
	}

	public function submitEditPosisi($id)
	{
		$this->form_validation->set_rules('job_title', 'Nama Job Title', 'required|trim');
		$this->form_validation->set_rules('divisi_departemen', 'Nama Divisi/Departemen', 'required|trim');
		$this->form_validation->set_rules('nama_posisi', 'Nama Posisi', 'required|trim');
		// $id = $this->input->post('id');

		$post_xss = $this->security->xss_clean($this->input->post());
		$decrypt_id = $this->encryption->decrypt(str_replace(['miringmiring', 'samasama', 'plusplus', 'et'], ['/', '=', '+', '@'], $id));

		$data['data'] = $this->Posisi_m->getPosisi($decrypt_id)->row_array();

		if ($this->form_validation->run() == FALSE) {

			$data['id'] = $id;
			$data['dataDivisiDepartemen'] = $this->Posisi_m->getDivisiDepartemen()->result_array();
			$data['dataJobTitle'] = $this->Posisi_m->getJobTitle()->result_array();
			$this->template('master_data/posisi/edit_posisi_v', $data);
			// redirect('Posisi/editPosisi/' . $id);
		} else {
			// $encrypt_email = $this->encryption->encrypt($post_xss["email"]);
			$data =	 [
				"JOB_TITLE_ID" => $post_xss["job_title"],
				"DIVISI_ID" => $post_xss["divisi_departemen"],
				"NAMA_POSISI" => $post_xss["nama_posisi"],
				"STATUS" => $post_xss["status"],
				//  "created_datetime" => date('Y-m-d H:i:s')
				//  "email" => $encrypt_email
			];

			$update = $this->Posisi_m->updatePosisi($decrypt_id, $data);

			if ($update) {

				$this->session->set_flashdata('status', 'Berhasil mengubah Posisi');
				redirect(site_url('Posisi'));
			} else {
				$this->session->set_flashdata('status', 'Gagal mengubah Posisi');
				$this->template('master_data/posisi/edit_posisi_v', $data);
				// redirect('Posisi/editPosisi/' . $id);
			}
		}
	}
}
