<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_m extends CI_Model {

	public function insertKelas($data){
		
		$this->db->insert('kelas', $data);

		return $this->db->affected_rows();
	}

	public function getKelas($id="", $limit=0, $offset=0, $search=""){
		$this->db->join('jurusan', 'jurusan.id = kelas.id_jurusan');
		$this->db->select('kelas.*, jurusan.nama_jurusan');
		
		if(!empty($id)){
			$this->db->where('kelas.id', $id);
		}

		if(!empty($search)){
			$this->db->group_start();
			$this->db->like('LOWER(nama_jurusan)', strtolower($search));
			$this->db->or_like('LOWER(kode_kelas)', strtolower($search));
			$this->db->or_like('LOWER(nama_kelas)', strtolower($search));
			$this->db->group_end();
		}

		return $this->db->order_by('kelas.id', 'asc')->get('kelas', $limit, $offset, $search);
	}

	public function getJurusan($id="", $limit=0, $offset=0){
		if(!empty($id)){
			$this->db->where('jurusan.id', $id);
		}
		return $this->db->order_by('jurusan.id', 'asc')->get('jurusan');
	}


	public function updateKelas($id, $data){
		$this->db->where('id', $id);
		return $this->db->update('kelas', $data);
		
	}

	public function deleteKelas($id){
		$this->db->where('id', $id);
		$this->db->delete('kelas');

		return $this->db->affected_rows();
	}	

}

/* End of file Kelas_m.php */
/* Location: ./application/models/Kelas_m.php */