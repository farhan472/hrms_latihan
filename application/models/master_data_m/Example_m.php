<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posisi_m extends CI_Model {

	public function insertPosisi($data){
		
		$this->db->insert('posisi', $data);

		return $this->db->affected_rows();
	}

	public function getPosisi($id="", $limit=0, $offset=0, $search=""){
		// $this->db->join('jurusan', 'jurusan.id = kelas.id_jurusan');
		// $this->db->select('posisi');
		
		if(!empty($id)){
			$this->db->where('id', $id);
		}

		if(!empty($search)){
			$this->db->group_start();
			$this->db->like('LOWER(nama_posisi)', strtolower($search));
			// $this->db->or_like('LOWER(kode_kelas)', strtolower($search));
			$this->db->group_end();
		}

		return $this->db->order_by('id', 'asc')->get('posisi', $limit, $offset, $search);
	}

	public function updatePosisi($id, $data){
		$this->db->where('id', $id);
		return $this->db->update('posisi', $data);
		
	}

}

/* End of file Kelas_m.php */
/* Location: ./application/models/Kelas_m.php */