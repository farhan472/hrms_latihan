<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job_title_m extends CI_Model {

	public function insertJobTitle($data){
		
		$this->db->insert('job_title', $data);

		return $this->db->affected_rows();
	}

	public function getJobTitle($id="", $limit=0, $offset=0, $search=""){
		// $this->db->join('jurusan', 'jurusan.id = kelas.id_jurusan');
		// $this->db->select('posisi');
		
		if(!empty($id)){
			$this->db->where('id', $id);
		}

		if(!empty($search)){
			$this->db->group_start();
			$this->db->like('LOWER(nama_jobtitle)', strtolower($search));
			// $this->db->or_like('LOWER(kode_kelas)', strtolower($search));
			$this->db->group_end();
		}

		return $this->db->order_by('id', 'asc')->get('job_title', $limit, $offset, $search);
	}

	public function updateJobTitle($id, $data){
		$this->db->where('id', $id);
		return $this->db->update('job_title', $data);
		
	}

}

/* End of file Kelas_m.php */
/* Location: ./application/models/Kelas_m.php */