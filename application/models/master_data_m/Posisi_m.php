<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posisi_m extends CI_Model {

	public function insertPosisi($data){
		
		$this->db->insert('ADM_POSISI', $data);

		return $this->db->affected_rows();
	}

	public function getPosisi($id="", $limit=0, $offset=0, $search=""){
		$this->db->join('ADM_DIVISI', 'ADM_DIVISI.ID = ADM_POSISI.DIVISI_ID');
		$this->db->join('ADM_JOB_TITLE', 'ADM_JOB_TITLE.ID = ADM_POSISI.JOB_TITLE_ID');
		$this->db->select('ADM_POSISI.*, ADM_DIVISI.NAMA_DIVISI, ADM_JOB_TITLE.NAMA_JOBTITLE');
		
		if(!empty($id)){
			$this->db->where('ADM_POSISI.ID', $id);
		}

		if(!empty($search)){
			$this->db->group_start();
			$this->db->like('LOWER(NAMA_POSISI)', strtolower($search));
			// $this->db->or_like('LOWER(kode_kelas)', strtolower($search));
			$this->db->group_end();
		}

		return $this->db->order_by('ADM_POSISI.ID', 'asc')->get('ADM_POSISI', $limit, $offset, $search);
	}

	public function getDivisiDepartemen(){

		return $this->db->get('ADM_DIVISI');
	}

	public function getJobTitle(){
	
		return $this->db->get('ADM_JOB_TITLE');
	}

	public function editPosisi($id){
		$this->db->where('ID', $id);

		return $this->db->get('ADM_POSISI');
	}
	

	public function updatePosisi($id, $data){
		$this->db->where('ID', $id);
		return $this->db->update('ADM_POSISI', $data);
		
	}

}

/* End of file Kelas_m.php */
/* Location: ./application/models/Kelas_m.php */