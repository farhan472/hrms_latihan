<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisi_departemen_m extends CI_Model {

	public function insertDivisiDepartemen($data){
		
		$this->db->insert('adm_divisi', $data);

		return $this->db->affected_rows();
	}

	public function getDivisiDepartemen($id="", $limit=0, $offset=0, $search=""){
		// $this->db->join('jurusan', 'jurusan.id = kelas.id_jurusan');
		// $this->db->select('posisi');
		
		if(!empty($id)){
			$this->db->where('id', $id);
		}

		if(!empty($search)){
			$this->db->group_start();
			$this->db->like('LOWER(nama_divisi)', strtolower($search));
            $this->db->or_like('LOWER(kode_divisi)', strtolower($search));
            $this->db->like('LOWER(status)', strtolower($search));
			$this->db->group_end();
		}

		return $this->db->order_by('id', 'asc')->get('adm_divisi', $limit, $offset, $search);
	}

	public function updateDivisiDepartemen($id, $data){
		$this->db->where('id', $id);
		return $this->db->update('adm_divisi', $data);
		
	}

}

/* End of file Kelas_m.php */
/* Location: ./application/models/Kelas_m.php */