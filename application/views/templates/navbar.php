<div class="container-fluid bg-white">
  <!-- <h1> Sederhana</h1> -->
  <nav class="navbar navbar-expand-lg navbar-light ">
    <a class="navbar-brand" href="<?php echo site_url() ?>">
      <h1>HR Aqmal Ganteng</h1>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="jurusanDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Talent
          </a>
          <div class="dropdown-menu" aria-labelledby="jurusanDropdown">
            <a class="dropdown-item" href="<?php echo site_url('') ?>">Karyawan</a>
            <a class="dropdown-item" href="<?php echo site_url('') ?>">Calon Karyawan</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="jurusanDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Master Data
          </a>
          <div class="dropdown-menu" aria-labelledby="jurusanDropdown">
            <a class="dropdown-item" href="<?php echo site_url('posisi') ?>">Posisi</a>
            <a class="dropdown-item" href="<?php echo site_url('job_title') ?>">Job Title</a>
            <a class="dropdown-item" href="<?php echo site_url('divisi_departemen') ?>">Divisi</a>
            <a class="dropdown-item" href="<?php echo site_url('') ?>">Template Email</a>
            <a class="dropdown-item" href="<?php echo site_url('') ?>">Tingkat Pendidikan Akhir</a>
            <a class="dropdown-item" href="<?php echo site_url('domisili') ?>">Domisili</a>
            <a class="dropdown-item" href="<?php echo site_url('hirarki') ?>">Hirarki</a>
            <a class="dropdown-item" href="<?php echo site_url('') ?>">Menu Management</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('About') ?>">About</a>
        </li>
      </ul>
      <ul class="navbar-nav navbar-right">
        <?php if ($this->session->userdata('nama')) { ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url(''); ?>">Logout</a>
          </li>
        <?php } else { ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url(''); ?>">Login</a>
          </li>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>

<!-- <div class="jumbotron"> -->
<div class="container-fluid" style="background-color:#e9ecef;">