
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Empty Page</title>

    <!-- <link href="font-awesome/css/all.css" rel="stylesheet"> -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-grid.min.css') ?>"  crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap-table/dist/bootstrap-table.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/fontawesome-5.14.0/css/fontawesome.min.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.resizableColumns.css') ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>"  crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/all.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css') ?>"  crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/now-ui-kit.css') ?>"  crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-table.css') ?>"  crossorigin="anonymous">
	

    <!-- Mainly scripts -->
	<script src="<?php echo base_url('assets/js/jquery.min.js') ?>" crossorigin="anonymous"></script>
	<script src="<?php echo base_url('assets/js/popper.min.js') ?>" crossorigin="anonymous"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>" crossorigin="anonymous"></script>
    <script src="<?php echo base_url('assets/js/plugins/metisMenu/jquery.metisMenu.js') ?>" crossorigin="anonymous"></script>
	<script src="<?php echo base_url('assets/js/plugins/slimscroll/jquery.slimscroll.min.js') ?>" crossorigin="anonymous"></script>
    
    
     
    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url('assets/js/inspinia.js') ?>" crossorigin="anonymous"></script>
	<script src="<?php echo base_url('assets/js/plugins/pace/pace.min.js') ?>" crossorigin="anonymous"></script>


    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="<?php echo base_url('assets/js/now-ui-kit.js?v=1.2.2" type="text/javascript') ?>" crossorigin="anonymous"></script>

</head>

<body>

    <div class="d-flex">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="d-flex">
                <div class="navbar-brand d-flex align-items-center justify-content-center">
                    <span style="color: #005eb596; font-weight:700; font-size:36px;">HRMS</span>
                </div>
                <div class="navbar-header d-flex align-items-center ">
                    <ul class="nav navbar-mainmenu">
                        <li class="mr-1"><a href="">Talent</a></li>
                        <li class="mr-1"><a href="">Pengajuan Cuti</a></li>
                        <li class="mr-1"><a href="">Asset</a></li>
                        <li class="mr-1"><a href="">e-KPI</a></li>
                        <li><a class="active" href="">Master Data</a></li>
                    </ul>
                    <!-- <form class="search-form">
                        <div class="form-group input-group no-border flex-nowrap">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-search"
                                        style="color: #878F92 ;"></i></span>
                            </div>
                            <input type="text" class="form-control " placeholder="Search...">
                        </div>
                    </form> -->
                </div>
            </div>
            <ul class="nav navbar-top-links navbar-right d-flex align-items-center flex-nowrap">
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="far fa-envelope"></i> <span class="label label-warning">16</span>
                    </a>
                    
                    <ul class="dropdown-menu">
                        <li>
                            <div class="dropdown-messages-box d-flex justify-content-center">
                                <a class="dropdown-item" href="#">
                                    1 Notification
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="far fa-bell"></i> <span class="label label-primary">8</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="mailbox.html" class="dropdown-item">
                                <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam, qui!</span>
                            </a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <a href="profile.html" class="dropdown-item">
                                New Message
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <img class="cog-icon" src="<?php echo base_url('assets/img/cog.svg') ?>" alt="cog-icon">
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="dropdown-messages-box d-flex justify-content-center">
                                <a class="dropdown-item" href="#">
                                    HRD
                                </a>
                                <div class="role d-flex justify-content-center align-items-center">
                                    <span>H</span>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <div class="dropdown-messages-box d-flex justify-content-center align-items-center">
                                <a class="dropdown-item" href="#">
                                    USER
                                </a>
                                <div class="role d-flex justify-content-center align-items-center">
                                    <span>U</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown profile d-flex flex-column">
                    <button class="btn-link d-flex align-items-center justify-content-between dropdown-toggle"
                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="fas fa-chevron-down"></span>
                        <span>Admin</span>
                        <img class="profile-img" src="<?php echo base_url('assets/img/a4.jpg') ?>" alt="">
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <li>
                            <a class="dropdown-item" href="#">Change Username</a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <a class="dropdown-item" href="#">Change Password</a>
                        </li>
                    </ul>
                </li>
            </ul>

        </nav>
    </div>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse d-flex flex-column">
                <ul class="nav metismenu" id="side-menu">
                    <li>
                        <h2>Master Data</h2>
                    </li>
                    </li>
                    <li class="nav-link">
                        <a href="<?= site_url('posisi') ?>"><span class="nav-label">Posisi</span></a>
                    </li>
                    <li class="nav-link">
                        <a href="<?= site_url('job_title') ?>"><span class="nav-label">job Title</span></a>
                    </li>
                    <li class="nav-link">
                        <a href="<?= site_url('divisi_departemen') ?>"><span class="nav-label">Divisi/Departement</span></a>
                    </li>
                    <li class="nav-link">
                        <a href="template_email.html"><span class="nav-label">Template Email</span></a>
                    </li>
                    <li class="nav-link">
                        <a href="tingkat_pddk.html"><span class="nav-label">Tingkat Pendidikan Terakhir</span></a>
                    </li>
                    <li class="nav-link">
                        <a href="domisili.html"><span class="nav-label">Domisili</span></a>
                    </li>
                    <li class="nav-link">
                        <a href="hirarki.html"><span class="nav-label">Hirarki</span></a>
                    </li>
                    <li class="nav-link">
                        <a href="proyek.html"><span class="nav-label">Proyek</span></a>
                    </li>
                    <li class="nav-link">
                        <a href="menu_management.html"><span class="nav-label">Menu Management</span></a>
                    </li>
                </ul>
                <ul class="nav metismenu">
                    <li class="nav-link">
                        <a href="login.html">
                            <img class="logout-icon" src="<?php echo base_url('assets/img/logout.svg') ?>" alt="">
                            <span class="logout">Log Out</span>
                        </a>
                    </li>
                    <li class="nav-link">
                        <span class="copy">&copy; 2020 ADW Consulting, All Rights Reserved</span>
                    </li>
                </ul>
            </div>
        </nav>
