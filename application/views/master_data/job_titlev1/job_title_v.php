
        <div id="page-wrapper">
            <div class="row wrapper border-bottom white-bg page-heading p-0 pb-4">
                <div class="col-sm-4 p-0">
                    <h2>Job Title</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">Master Data</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Job Title</strong>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="wrapper wrapper-content">
                <div id="toolbar" >
		            
		            <span class='pull-left'>
		            	<!-- <a href="<?= site_url("posisi/tambahPosisi") ?>" class='btn btn-sm btn-primary'
		            	style='margin: 0 5px 0 5px;padding: 7px' >
		            	Tambah Posisi
                        </a> -->
                        <div class="add d-flex align-items-center">
                            <a href="<?= site_url("job_title/tambahJobTitle") ?>" class="d-flex align-items-center">
                                <div class="rounded-circle inline-block d-flex justify-content-center align-items-center">
                                  <i class="fas fa-plus"></i>
                                </div>
                            <span>Tambah Job Title</span>
                            </a>
                        </div>
		            </span>
		        </div>

                <table class="table border-0" 
                id="tablejobtitle" 
                data-toggle="table" 
                data-search="true"
                data-show-refresh="true" 
                data-pagination="true" 
                data-side-pagination="server" 
                data-show-pagination-switch="true"
                data-detail-formatter="detailFormatter" 
                data-show-export="true"
                data-export-types="['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf']" 
                data-export-options='{"fileName": "data_JobTitle"}'
                data-click-to-select="true" 
                data-url="<?= site_url("job_title/dataJobTitle") ?>" 
                data-page-size="5"
				data-page-list="[5, 10, 25, 50, All]">
                    <thead>
                        <tr>
                            <th data-formatter="numberFormatter">No</th>
                            <th data-field="nama_jobtitle" data-sortable="true">Nama Job Title</th>
                            <th data-formatter="actionFormatter" data-field="id" data-force-hide="true" class="text-center" data-width="200">Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <script>
        function numberFormatter(value, row, index) {
            var options = $('#tablejobtitle').bootstrapTable('getOptions')
            // alert(options["pageNumber"] + " " + options["pageSize"])
            //  console.log(options["pageSize"]);
            var tes = 0
            if (!isNaN(options['pageSize'])) { //cek pagesize angka atau tidak, klo angka jalankan kode dibawah
                tes = ((options["pageNumber"] - 1) * options["pageSize"])
            }
            return index + 1 + tes;
        }

        function actionFormatter(value, row, index) {
            return [
                // '<a href="<?= site_url("Kelas/detail_kelas/") ?>' + value + '" class="btn btn-primary ">Detail</a>',
                // ' ',
                '<a href="<?= site_url("job_title/editJobTitle/") ?>' + value + '" class="btn btn-success ">Edit</a>',
                ' ',
                '<a href="<?= site_url("posisi/delete_kelas/") ?>' + value + '" class="btn btn-danger"  >Hapus</a>',
            ].join('');
        }
    </script>
</body>

</html>