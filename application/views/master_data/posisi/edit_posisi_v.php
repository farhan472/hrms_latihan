<div id="page-wrapper">
	<div class="wrapper border-bottom white-bg page-heading p-0 pb-4">
		<div class="col-sm-4 p-0">
			<h2>Posisi</h2>
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="<?php echo site_url('posisi') ?>">Master Data</a>
				</li>
				<li class="breadcrumb-item ">
					<a href="<?php echo site_url('posisi') ?>">Posisi</a>
				</li>
				<li class="breadcrumb-item active">
					<strong>Edit Posisi</strong>
				</li>
			</ol>
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRightBig">
		<div class="middle-box editPage">
			<h3 class="border-bottom">Edit Posisi</h3>
			<form action="<?php echo site_url('posisi/submitEditPosisi/'.$id) ?>" method="POST"
            enctype="multipart/form-data">

				<div class="form-row">
					<div class="col-md-6">
						<label class="control-label">Nama Posisi :</label>
						
						<?php $curval =  set_value("nama_posisi") ? set_value("nama_posisi") : $data['NAMA_POSISI'] ?>
						<input type="text" name="nama_posisi" class="form-control no-border"
                            value="<?php echo $curval ?>">
                        <?php echo form_error('nama_posisi', '<div class="text-danger">', '</div>'); ?>
						<br>
						
					</div>
					<div class="col-md-6">
						<label>Nama Divisi Departemen : </label>
						<select name="divisi_departemen" class="form-control no-border">
							<?php foreach($dataDivisiDepartemen as $divisiDepartemen):?>
							<option value="<?php echo $divisiDepartemen['ID']?>"
								<?php if($data['DIVISI_ID'] == $divisiDepartemen['ID'])echo 'selected' ?>>
								<?php echo $divisiDepartemen['NAMA_DIVISI']?>
							</option>
							<?php endforeach; ?>
						</select>
						<?php echo form_error('divisi_departemen', '<div class="text-danger">', '</div>'); ?>
					</div>
					<div class="col-md-6">
						<label>Nama Job Title : </label>
						<select name="job_title" class="form-control no-border">
							<?php foreach($dataJobTitle as $jobTitle):?>
							<option value="<?php echo $jobTitle['ID']?>"
								<?php if($data['JOB_TITLE_ID'] == $jobTitle['ID'])echo 'selected' ?>>
								<?php echo $jobTitle['NAMA_JOBTITLE']?>
							</option>
							<?php endforeach; ?>
						</select>
						<?php echo form_error('job_title', '<div class="text-danger">', '</div>'); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="col-12">
						<span>Status</span>
					</div>
					<div class="form-check form-check-radio form-check-inline">
						<label class="form-check-label">
							<input class="form-check-input" name="status" type="radio" value="Aktif"
								<?php echo ($data["STATUS"] == 'Aktif' ? ' checked' : ''); ?>> Aktif
							<span class="form-check-sign"></span>
						</label>
					</div>
					<div class="form-check form-check-radio form-check-inline">
						<label class="form-check-label">
							<input class="form-check-input" name="status" type="radio" value="Nonaktif"
								<?php echo ($data["STATUS"] == 'Nonaktif' ? ' checked' : ''); ?>> Nonaktif
							<span class="form-check-sign"></span>
						</label>
					</div>
				</div>
				<div class="action clearfix">
					<button class="btn simpan float-right">Edit</button>
					<button class="btn kembali float-right" type="submit">
						<a href="<?= site_url("posisi") ?>">Kembali</a>
					</button>
				</div>
    		</div>
        </form>
    </div>
</div>
</div>