
     <div id="page-wrapper">
        <div class="wrapper border-bottom white-bg page-heading p-0 pb-4">
            <div class="col-sm-4 p-0">
                <h2>Posisi</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo site_url('posisi') ?>">Master Data</a>
                    </li>
                    <li class="breadcrumb-item ">
                        <a href="<?php echo site_url('posisi') ?>">Posisi</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Tambah Posisi</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRightBig">
            <div class="middle-box addPage">
                    <h3 class="border-bottom">Tambah Posisi</h3>
                    <form action="<?php echo site_url('posisi/submitTambahPosisi') ?>" method="POST" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="namaPosisi">Nama Posisi</label>
                                <input type="text" name="nama_posisi" id="namaPosisi" class="form-control no-border" placeholder="Masukkan nama Posisi" 
                                value="<?php echo set_value("nama_posisi") ? set_value("nama_posisi") : ''  ?>">
                                
                                <?php echo form_error('nama_posisi', "<span>", "</span>"); ?>
                            </div>
                            <div class="col-md-6">
                                <label>Nama Divisi/Departement</label>
                                
                                <select name="divisi_departemen" id="divisi_departemen" class="form-control">
				                  <?php foreach($dataDivisiDepartemen as $divisiDepartemen):?>
				                		<option value="<?php echo $divisiDepartemen['ID']?>" >
				                		<?php echo $divisiDepartemen['NAMA_DIVISI']?>
				                    	</option>
				                	<?php endforeach; ?>
				                </select> 
				                <?php echo form_error('divisi_departemen', "<span>", "</span>"); ?>
                            </div>
                        </div>
                        <div class="form-row">
                                <div class="col-md-6">
                                    <label>Nama Job Title</label>
                                    
                                    <select name="job_title" id="divisi_departemen" class="form-control">
				                      <?php foreach($dataJobTitle as $jobTitle):?>
				                    		<option value="<?php echo $divisiDepartemen['ID']?>" >
				                    		<?php echo $jobTitle['NAMA_JOBTITLE']?>
				                        	</option>
				                    	<?php endforeach; ?>
				                    </select> 
				                    <?php echo form_error('job_title', "<span>", "</span>"); ?>
                                </div>
                            </div>
                        <div class="action clearfix">
                            <button class="btn simpan float-right">Simpan</button>
                            <button class="btn kembali float-right" type="submit">
                                <a href="<?= site_url("posisi") ?>">Kembali</a>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>

</html>