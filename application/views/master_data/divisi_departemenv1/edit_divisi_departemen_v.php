
        <div id="page-wrapper">
            <div class="row wrapper border-bottom white-bg page-heading p-0 pb-4">
                <div class="col-sm-4 p-0">
                    <h2>Divisi / Departemen</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">Master Data</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Divisi / Departemen</strong>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="wrapper wrapper-content">
                <div class="container">
	            	<h2>Edit Divisi</h2>
	            	<?php echo $this->session->flashdata('status'); ?>
                </div>
                
                <div class="card-body">
                    <form action="<?php echo site_url('divisi_departemen/submitEditDivisiDepartemen') ?>" method="POST" enctype="multipart/form-data">
                        <div class="form-body">
                            <div class="row ">
                                <div class="col-md-12">
                                    
                                    <div class="form-group">
                                        <label class="control-label">Nama Divisi :</label>
                                        <?php $curval =  set_value("id") ? set_value("id") : $data['id']?>
				                        <input type="hidden" name="id" value="<?php echo $curval ?>">
			                        	<?php $curval =  set_value("nama_divisi") ? set_value("nama_divisi") : $data['nama_divisi']?>
			                        	<input type="text" name="nama_posisi" class="input" value="<?php echo $curval ?>">
                                        <br>
			                        	<?php echo form_error('nama_divisi', "<span>", "</span>"); ?>
			                        </div>
                                    <div class="form-group">
                                        <label class="control-label">Kode Divisi :</label>
			                        	<?php $curval =  set_value("kode_divisi") ? set_value("kode_divisi") : $data['kode_divisi']?>
			                        	<input type="text" name="kode_divisi" class="input" value="<?php echo $curval ?>">
                                        <br>
			                        	<?php echo form_error('kode_divisi', "<span>", "</span>"); ?>
			                        </div>
                                    
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Edit</button>
                            <a href="<?= site_url("divisi_departemen") ?>" type="submit" class="btn btn-info">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function numberFormatter(value, row, index) {
            var options = $('#tablekelas').bootstrapTable('getOptions')
            // alert(options["pageNumber"] + " " + options["pageSize"])
            //  console.log(options["pageSize"]);
            var tes = 0
            if (!isNaN(options['pageSize'])) { //cek pagesize angka atau tidak, klo angka jalankan kode dibawah
                tes = ((options["pageNumber"] - 1) * options["pageSize"])
            }
            return index + 1 + tes;
        }

        function actionFormatter(value, row, index) {
            return [
                '<a href="<?= site_url("Kelas/detail_kelas/") ?>' + value + '" class="btn btn-primary ">Detail</a>',
                ' ',
                '<a href="<?= site_url("Kelas/updateform/") ?>' + value + '" class="btn btn-success ">Edit</a>',
                ' ',
                '<a href="<?= site_url("Kelas/delete_kelas/") ?>' + value + '" class="btn btn-danger"  >Hapus</a>',
            ].join('');
        }
    </script>

</body>

</html>