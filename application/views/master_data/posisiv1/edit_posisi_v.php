<div id="page-wrapper">
    <div class="row wrapper border-bottom white-bg page-heading p-0 pb-4">
        <div class="col-sm-4 p-0">
            <h2>Posisi</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html">Master Data</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Posisi</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="container">
            <h2>Edit Posisi</h2>
        </div>

        <div class="card-body">
            <form action="<?php echo site_url('posisi/submitEditPosisi') ?>" method="POST" enctype="multipart/form-data">
                <div class="form-body">
                    <div class="row ">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="control-label">Nama Posisi :</label>
                                <?php $curval =  set_value("id") ? set_value("id") : $data['id'] ?>
                                <input type="hidden" name="id" value="<?php echo $curval ?>">
                                <?php $curval =  set_value("nama_posisi") ? set_value("nama_posisi") : $data['nama_posisi'] ?>
                                <input type="text" name="nama_posisi" class="input" value="<?php echo $curval ?>">
                                <br>
                                <?php echo form_error('nama_posisi', "<span>", "</span>"); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email :</label>
                                <?php $curval =  set_value("email") ? set_value("email") : $this->encryption->decrypt($data['email']) ?>
                                <input type="text" name="email" class="input" value="<?php echo $curval ?>">
                                <br>
                                <?php echo form_error('email', "<span>", "</span>"); ?>
                            </div>

                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Edit</button>
                    <a href="<?= site_url("posisi") ?>" type="submit" class="btn btn-info">Kembali</a>
                </div>

            </form>
        </div>
    </div>
</div>
</div>
<script>
    function numberFormatter(value, row, index) {
        var options = $('#tablekelas').bootstrapTable('getOptions')
        // alert(options["pageNumber"] + " " + options["pageSize"])
        //  console.log(options["pageSize"]);
        var tes = 0
        if (!isNaN(options['pageSize'])) { //cek pagesize angka atau tidak, klo angka jalankan kode dibawah
            tes = ((options["pageNumber"] - 1) * options["pageSize"])
        }
        return index + 1 + tes;
    }

    function actionFormatter(value, row, index) {
        return [
            '<a href="<?= site_url("Kelas/detail_kelas/") ?>' + value + '" class="btn btn-primary ">Detail</a>',
            ' ',
            '<a href="<?= site_url("Kelas/updateform/") ?>' + value + '" class="btn btn-success ">Edit</a>',
            ' ',
            '<a href="<?= site_url("Kelas/delete_kelas/") ?>' + value + '" class="btn btn-danger"  >Hapus</a>',
        ].join('');
    }
</script>

</body>

</html>